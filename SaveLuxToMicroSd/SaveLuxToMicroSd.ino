#include <SD.h>
#include <SPI.h>
#include <Wire.h> // Library for I2C communication -> lcd
#include <LiquidCrystal_I2C.h> // Library for LCD

#include <OneWire.h>  // i2c for dallas
#include <DallasTemperature.h>

#include <DS1302.h>
DS1302 rtc(5, 6, 7);



//config for sd
static boolean enable2save = false;
File myFile;
int pinCS = 4;
static String fileName = "";
int i = 0;

const int bufferN = 10; // must be greater than 2
double bufferLux[bufferN] = {};
double bufferTemperature[bufferN] = {};
int totalDelaySeconds = 30; // must be greater than 0
int delayMs = (totalDelaySeconds / bufferN) * 1000;

//L=BR^m
//config for 0-1024 to Lux
const double k = 5.0 / 1024;
const double R2 = 10000;
const double B = pow(10.0, 8.40);
const double m = -1.33;


#define onewirepin A2
OneWire oneWire(onewirepin);
DallasTemperature sensors(&oneWire);

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 20, 4);


void setup() {
  Serial.begin(9600);
  pinMode(pinCS, OUTPUT);
  if (SD.begin(4)) {
    enable2save = true;
    fileName = createFile(fileName);
       Serial.println(fileName);

    Serial.println("The data will be saved to " + fileName);
  } else {
    Serial.println("SD card initialization failed -> nothing will be saved");
  }

  lcd.init();
  lcd.backlight();
  //
  //    rtc.halt(false);
  //    rtc.writeProtect(false);
  //    rtc.setDOW(MONDAY);
  //    rtc.setTime(21, 50, 00);
  //    rtc.setDate(8, 3, 2021);
  //    rtc.writeProtect(true);

  sensors.begin();
}

String createFile(String fileName) {
  String header = "date, time, LUX, temperature (C)";
  Time times = rtc.getTime();
  
  String data = String(times.mon)+String(times.date)+String(times.hour)+String(times.min);
   fileName = data+".csv";
   Serial.println(fileName);
    if (!SD.exists(fileName)) {
      File myFile = SD.open(fileName, FILE_WRITE);
      if (myFile) {
        Serial.println("Writing FILE " + fileName);
        myFile.println(header);
        myFile.close(); 
      }
    }
    return fileName;
}



void writeFile(String lux, String temperature) {
  String body = String(rtc.getDateStr(FORMAT_LONG, FORMAT_LITTLEENDIAN, '/')) + "," + String(rtc.getTimeStr()) + "," + lux + "," + temperature;
  if (enable2save == true) {
    File myFile = SD.open(fileName, FILE_WRITE);
    if (myFile) {
      char saveBody[sizeof(body)];
      myFile.println(body);
      myFile.close();
    } else {
    }
  }
}

double getLux (int a0) {
  double V2 = k * a0; // V = IR
  double R1 = (5.0 / V2 - 1) * R2;
  double powe = pow(R1, m);
  double lux = B * pow(R1, m); 
  return lux;
}

double getTemperature() {
  sensors.requestTemperatures();
  return sensors.getTempCByIndex(0);
}

void show2screen(String a0, String lux, String currentBuffer, String currentTemperature) {
  lcd.clear();
  lcd.setCursor(0, 0);
  if (enable2save == true) {
    lcd.print("Saving to: " + fileName);
  } else {
    lcd.print("sd not configured");
  }
  lcd.setCursor(0, 1);
  lcd.print("temperature: " + currentTemperature);
  lcd.setCursor(0, 2);
  lcd.print("Analog read " + a0);
  lcd.setCursor(0, 3);
  lcd.print("Lux " + lux + " - " + currentBuffer + " of " + String(bufferN, DEC));
}


double getAvgWithoutMaxAndMin(double bufferLux[]) {
  double sumAllElements = 0;
  double maxLux = -1;
  double minLux = 1025;
  int i = 0;
  while (i < bufferN) {
    sumAllElements = sumAllElements + bufferLux[i];
    if (maxLux < bufferLux[i]) {
      maxLux = bufferLux[i];
    }
    if (minLux > bufferLux[i]) {
      minLux = bufferLux[i];
    }
    i ++;
  }

  double diff = maxLux + minLux;
  int nElements = (double) (bufferN - 2);
  sumAllElements = sumAllElements - diff;
  double res = ((double) sumAllElements) /  (double)(bufferN - 2);
  return res;
}

void loop() {
  int bufferExecuted = 0;

  while (bufferExecuted < bufferN) {
    int a0 = analogRead(3);
    double intensity = getLux(a0);
    double temperature = getTemperature();
    bufferLux[bufferExecuted] = intensity;
    bufferTemperature[bufferExecuted] = temperature;
    bufferExecuted ++;
    show2screen(String(a0, DEC), String(intensity, 0), String(bufferExecuted, DEC), String(temperature, 1));
    delay(delayMs);
  }
  double luxDouble = getAvgWithoutMaxAndMin(bufferLux);
  double temperatureDouble = getAvgWithoutMaxAndMin(bufferTemperature);

  String lux = String(luxDouble, 3);
  String temperature = String(temperatureDouble, 2);
  writeFile(lux, temperature);

  Serial.println("[" + String(rtc.getTimeStr()) + "] Got lux: " + lux +" and temperature: " +temperature);
}
